import logging
import os
from Papers import Papers


def log_file_name():
    from datetime import datetime
    timestamp = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    return f"{os.getenv('HOME')}/work/thrive-{timestamp}.log"


handlers = [logging.FileHandler(log_file_name()), logging.StreamHandler()]


logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.INFO, handlers=handlers)


def scrape_article(driver, title, article_url, visited_titles=set()):
    """Scrapes the target article data

    Verifies if the driver should be scraped and, if it should, it scrapes it.

    Args: 
        driver (webdriver): selenium driver to interact with
        title (str): title of the article to scrape
        article (str): url of article to scrape
        visited_titles (set): set of already visited titles
    Returns:
        visited_titles (set): Set of visited articles, it add `title` if it
        wasn't already visited
        articles with title `title` if it wasn't already scraped

    """

    def get_text(driver, selector):
        """Gets the text using the selector passed

        Args:
            driver (webdriver): driver used to select the data
            selector (str): selector to use

        Returns:
            selected_text (str): text from the selected element
        """
        try:
            return driver.find_element_by_css_selector(selector).text
        except Exception:
            return ''

    logging.info(f"About to scrape {title}")

    # If the title hasn't been visited
    if title not in visited_titles:

        # We get to the article only now to save a request
        driver.get(article_url)

        # article = driver.find_element_by_css_selector('div.rprt_all')

        # Collect data
        abstract = get_text(driver, 'div.abstr>div>p')

        # Now that we have the data, we append it to the articles list

        article_to_save = {
            'title': title,
            'abstract': abstract,
            'url': article_url
        }

        Papers.add_row(article_to_save)
        logging.info(f"Added {article_to_save['title']} to dataframe")

        driver.back()


def scrape_query(driver, query):
    """Scrapes the query

    Scrapes each article return from the query `query`

    Args: 
        driver (webdriver): selenium driver to interact with
        query (str): query to do and scrape from
        visited_titles (set): set of already visited titles
    Returns:
        visited_titles (set): Set of visited articles. It adds to the
        already visited titles the titles of the articles scraped
    """

    logging.info(f"Scraping {query}")

    # Go to the query site
    driver.get(query)

    while True:

        # First link
        link_nmbr = 0

        while True:

            articles = driver.find_elements_by_css_selector('div.rslt>p>a')

            visited_titles = Papers.get_titles()
            # Go to every article link collecting and adding to the articles
            # list and the set of visited articles

            scrape_article(
                driver,
                articles[link_nmbr].text,
                articles[link_nmbr].get_property('href'),
                visited_titles=visited_titles,
            )

            link_nmbr += 1

            # If we went through every article the exit
            if len(articles) == link_nmbr:
                break

        Papers.save_dataframe()
        logging.info("Data saved to disk")
        # This will exit if we find the Next button inactive
        # meaning we reached the last page of results
        try:
            # Comment this to only scrape the first page
            # Uncomment at your own risk
            driver.find_element_by_css_selector('.next.inactive')
            break
        except Exception:
            pass

        # Get the next button button
        elem = driver.find_element_by_css_selector('.next')

        # Click it
        elem.click()


def scrape_data(base_query_url, queries=[]):
    """Scrapes data from the base_query_url applying the listed queries

    args
    """

    from selenium import webdriver
    from selenium.webdriver.common.keys import Keys

    # Create options object for the browser
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('no-sandbox')

    # Instantiate browser to scrape
    driver = webdriver.Chrome(options=options)

    # For each of the terms
    for query in queries:
        # Replace spaces for + signs
        query = query.replace(' ', '+')

        # Build query
        url = base_query_url + query

        # Visit query and scrape
        scrape_query(
            driver,
            url
        )
        Papers.save_dataframe()
        logging.info("Data saved to disk")

    # Close browsekype
    driver.close()
