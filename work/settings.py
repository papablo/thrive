BASE_URL = 'http://www.ncbi.nlm.nih.gov'

BASE_QUERY_URL = BASE_URL + '/pubmed/?term='

QUERIES = [
    "(Maternal* OR Paternal*) AND (weight OR obes* OR nutrition OR diet* OR stress OR social support) AND (child OR infant) AND (Programming AND Development)",
    "Mother-child relation* AND Programming",
    "Child* AND development AND Programming AND (stress OR depression OR anxiety OR sensitivity OR temperament) AND mental health",
    "Parent-child relation* AND Programming",
    "Programming AND natal",
    "Development* AND Origins AND Programming",
    "(Maternal* OR Paternal*) AND (gene* OR immune* OR metabol* OR inflam* OR brain OR neuro* OR cardio* respiratory) AND (development OR growth OR Programming) AND (child OR infant)",
    "Development* Origins of Health and Disease",
]

DATA_FILE = 'data/papers.csv'