import os
import glob
import pandas as pd
from typing import List
from datetime import datetime


from settings import DATA_FILE


class Papers:

    __dataframe: pd.DataFrame = None

    @classmethod
    def get_dataframe(cls, *args, **kwargs) -> pd.DataFrame:
        # If the data file is not valid
        if os.path.isfile(DATA_FILE):

            # Check if I have a dataframe, if not, load it
            if cls.__dataframe is None:
                cls.__dataframe = pd.read_csv(DATA_FILE)[['title', 'abstract', 'url']]

            # Return dataframe
            return cls.__dataframe
        else:
            raise DataFileNotValidException("Data file is not valid")

    @classmethod
    def save_dataframe(cls, save_copy=False):

        # Get dataframe
        dataframe = cls.get_dataframe()

        if save_copy:
            papers = sorted(glob.glob('data/papers-*.csv'),
                            key=os.path.getmtime, reverse=True)

            if len(papers) > 0:

                path_last = papers[0]

                last_dataframe = pd.read_csv(path_last)

                if len(dataframe) != len(last_dataframe):
                    # save copy (for tracebility reasons)
                    path, extension = DATA_FILE.split('.')
                    timestamp = datetime.strftime(
                        datetime.now(), '%Y%m%d%H%M%S')

                    dataframe.to_csv(
                        f"{path}-{timestamp}.{extension}", index=False)


        dataframe.to_csv(DATA_FILE, index=False)

        return dataframe

    @classmethod
    def add_row(cls, row):
        dataframe = cls.get_dataframe()
        dataframe = dataframe.append(row, ignore_index=True)

        cls.__dataframe = dataframe

        return dataframe

    @classmethod
    def get_titles(cls) -> List[str]:
        dataframe = cls.get_dataframe()

        return [row['title'] for _, row in dataframe.iterrows()]


class DataFileNotValidException(Exception):
    pass
