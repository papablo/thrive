from typing import Tuple
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet as wn
import gensim
import pickle
import nltk
import logging
import os


def log_file_name():
    from datetime import datetime
    timestamp = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
    return f"{os.getenv('HOME')}/work/thrive-{timestamp}.log"


handlers = [logging.FileHandler(log_file_name()), logging.StreamHandler()]

logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.INFO, handlers=handlers)


nltk.download('wordnet')


def tokenize(text):
    import spacy
    spacy.load('en_core_web_sm')

    from spacy.lang.en import English

    lda_tokens = []

    parser = English()

    tokens = parser(text)
    for token in tokens:
        if token.orth_.isspace():
            continue
        elif token.like_url:
            lda_tokens.append('URL')
        elif token.orth_.startswith('@'):
            lda_tokens.append('SCREEN_NAME')
        else:
            lda_tokens.append(token.lower_)
    return lda_tokens


def get_lemma(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


def get_lemma2(word):
    return WordNetLemmatizer().lemmatize(word)


nltk.download('stopwords')
en_stop = set(nltk.corpus.stopwords.words('english'))


def prepare_text_for_lda(text):
    tokens = tokenize(text)
    tokens = [token for token in tokens if len(token) > 4]
    tokens = [token for token in tokens if token not in en_stop]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


# ## Useful links
#
# [Article doing an example in Python](https://towardsdatascience.com/topic-modelling-in-python-with-nltk-and-gensim-4ef03213cd21)
#
# [YouTube vid with a talk explaning topic models](https://www.youtube.com/watch?v=3mHy4OSyRf0)
#

def generate_model(corpus, dictionary, num_topics, suffix=''):
    from gensim.models.ldamodel import LdaModel

    # Create the model
    logging.info(f"About to create model. Picking [{num_topics}] topics")
    ldamodel = LdaModel(
        corpus,
        num_topics=num_topics,
        id2word=dictionary,
        passes=15)

    logging.info("Model created")
    logging.info("Saving model...")
    ldamodel.save(f'gen-models/model{num_topics}-{suffix}.gensim')

    return corpus, dictionary, ldamodel


def generate_dictionary(documents, suffix=''):
    from gensim import corpora

    logging.info(f'Got the data, they are [{len(documents)}] documents')

    # Create a set of data and create a dictionary from it
    logging.info("Preparing data")
    text_data = [prepare_text_for_lda(
        f"{d[0]} - {d[1]}") for d in documents]

    logging.info("Data prepared")

    logging.info("Creating dictionary")
    dictionary = corpora.Dictionary(text_data)
    corpus = [dictionary.doc2bow(text) for text in text_data]

    logging.info("Dictionary created")

    logging.info("Pickling dictionary")
    pickle.dump(corpus, open(f'gen-models/corpus-{suffix}.pkl', 'wb'))
    dictionary.save(f'gen-models/dictionary-{suffix}.gensim')
    logging.info("Dictionary pickled!")

    return corpus, dictionary


def model_topics(num_topics, corpus=None, dictionary=None, documents=None, suffix=''):
    """ Model the topics using `num_topics` number of topics

    It generates de necessary dictionary, corpus and returns the generated model
    """

    if not all([corpus, dictionary]):
        logging.info("I have no corpus nor dictionary!")
        logging.info("I'll generate them")
        corpus, dictionary = generate_dictionary(documents, suffix)

    return generate_model(corpus, dictionary, num_topics, suffix)


def visualize_model(corpus, dictionary, model):
    from pyLDAvis.gensim import prepare

    lda_display = prepare(
        model, corpus, dictionary, sort_topics=False,  mds='mmds')

    return lda_display
