import logging
import os
logger = logging.getLogger('thrive')
hdlr = logging.FileHandler(f'{os.getenv("HOME")}work/thrive.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)

