# thrive

thrive - Scraping and Natural Language Analysis

This repo contains all the tooling necessary to scrape, store and analyze the data we need, it uses Docker, and Jupyter Notebooks

# Running this project

Run 

```
$ docker-compose up
```

from inside the project folder and it will build everything for you. When it's done just copy the url that it gives you, with the token, so you can access the notebook server

**NOTE:** If you open this project in [vscode]() you can open it inside a container, for testing and debug purposes.

# What does this thing do?

First, it will scrape the data we need. This may take some time, since it will go through every search term in the `QUERIES` constant in the `work/settings.py` file. After the scraping is done, it will create a propicious environment for the natural language analysis and save the corpus (dictionary) and the generated models in the `work/gen-models` folder.
